#include <iostream>

using namespace std;


int main()
{
  int x = 25, y = 2 * x;    
  auto a = &x, b = a;
  auto c = *a, d = 2 * *b;


  cout<< endl <<"x = "<<x<<", y = "<<y<<endl;

  cout << "Variable A is of data type: Reference" << endl;
  cout << "It's value is: " << *a << endl << "It's address is: " << &a << endl << endl;

  cout << "Variable B is of data type: Reference" << endl;
  cout << "It's value is: " << *b << endl << "It's address is: " << &b << endl << endl;
  
  cout << "Variable C is of data type: Pointer" << endl;
  cout << "It's value is: " << c << endl << "It's address is: " << &c << endl << endl;
  
  cout << "Variable D is of data type: Integer" << endl;
  cout << "It's value is: " << d << endl << "It's address is: "<< &d << endl << endl;


}
